<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $100 = floor($amount/100); $amount= $amount -$100*100; //finds the highest number of 100's that can be subtracted, then subtracts that amount (updates the variable)
        $50 = floor($amount/50); $amount = $amount - $50*50;
        $20 = floor($amount/20); $amount=$amount-$20*20;
        $10 = floor($amount/10); $amount=$amount-$10*10;
        $5 = floor($amount/5); $amount=$amount-$5*5;
        $2 = floor($amount/2); $amount=$amount-$2*2;
        $1 = $amount;
        return array(
            '100' => $100,
            '50' => $50,
            '20' => $20,
            '10' => $10,
            '5' => $5,
            '2' => $2,
            '1' => $1,
        )
    }
}