<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        
        $stringFromFile = file_get_contents($filePath); //Get string from specified file path
        return preg_replace("/[^a-zA-Z]+/", "", $stringFromFile); //replace non-letter characters with nothing, thus leaving only letters.

    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
    
    $letterOccurences = array(
        "a" => strlen(preg_replace(/[^aA]+/, "", $parsedFile)), //removes any non-a letters from the string, then pairs "a" with the length of that string, i.e., the number of a�s. Same method applied below
        "b" => strlen(preg_replace(/[^bB]+/, "", $parsedFile)),
        "c" => strlen(preg_replace(/[^cC]+/, "", $parsedFile)),
        "d" => strlen(preg_replace(/[^dD]+/, "", $parsedFile)),
        "e" => strlen(preg_replace(/[^eE]+/, "", $parsedFile)),
        "f" => strlen(preg_replace(/[^fF]+/, "", $parsedFile)),
        "g" => strlen(preg_replace(/[^gG]+/, "", $parsedFile)),
        "h" => strlen(preg_replace(/[^hH]+/, "", $parsedFile)),
        "i" => strlen(preg_replace(/[^iI]+/, "", $parsedFile)),
        "j" => strlen(preg_replace(/[^jJ]+/, "", $parsedFile)),
        "k" => strlen(preg_replace(/[^kK]+/, "", $parsedFile)),
        "l" => strlen(preg_replace(/[^lL]+/, "", $parsedFile)),
        "m" => strlen(preg_replace(/[^mM]+/, "", $parsedFile)),
        "n" => strlen(preg_replace(/[^nN]+/, "", $parsedFile)),
        "o" => strlen(preg_replace(/[^oO]+/, "", $parsedFile)),
        "p" => strlen(preg_replace(/[^pP]+/, "", $parsedFile)),
        "q" => strlen(preg_replace(/[^qQ]+/, "", $parsedFile)),
        "r" => strlen(preg_replace(/[^rR]+/, "", $parsedFile)),
        "s" => strlen(preg_replace(/[^sS]+/, "", $parsedFile)),
        "t" => strlen(preg_replace(/[^tT]+/, "", $parsedFile)),
        "u" => strlen(preg_replace(/[^uU]+/, "", $parsedFile)),
        "v" => strlen(preg_replace(/[^vV]+/, "", $parsedFile)),
        "x" => strlen(preg_replace(/[^xX]+/, "", $parsedFile)),
        "y" => strlen(preg_replace(/[^yY]+/, "", $parsedFile)),
        "z" => strlen(preg_replace(/[^zZ]+/, "", $parsedFile))
    );
    asort($letterOccurences); //sort the array to find the median (middle value)
    $keys = array_keys($letterOccurrences); //getting keys into an indexed array to find the middle key
    return $keys[12];

        
    }
}